import React, { useState } from "react"
import { BrowserRouter as Router, Switch, Route } from "react-router-dom"
import { ThemeProvider } from "styled-components"
import GlobalStyle, {light, dark} from "./general-styles/GeneralStyles"
import Container from "./components/Container"
import HeaderSection from "./components/HeaderSection"
import NavSection from "./components/NavSection"
import FooterSection from "./components/FooterSection"
import Inicio from "./pages/Inicio"
import Mas from "./pages/Mas"

// configuracion del tema
export const AppTheme = React.createContext()

function App() {
	const [theme, setTheme] = useState({ light: "true" })
	/*const toggleMode = () => {
    console.log(theme)
    console.log(theme.light)
    setTheme( oldTheme => oldTheme.light ? {dark:'true'} : {light: 'true'}  )
    console.log(theme)
  }*/
	return (
		<AppTheme.Provider value={{ theme, setTheme }}>
			<ThemeProvider theme={theme.light ? light : dark }>
				{/*<button onClick={toggleMode} >dark/light mode</button>*/}
				<GlobalStyle {...theme} />
				<Container>
					<HeaderSection />
					<Router>
						<NavSection />
						<main>
							<Switch>
								<Route component={Mas} path="/mas" />
								<Route component={Inicio} path="/" exact />
							</Switch>
						</main>
					</Router>
					<FooterSection />
				</Container>
			</ThemeProvider>
		</AppTheme.Provider>
	)
}

export default App
