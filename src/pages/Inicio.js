import { useEffect, useState } from 'react'
import Col from '../components/Col'
import Row from '../components/Row'
import PokemonCard from '../components/PokemonCard'

const Inicio = () => {
    const [pokemons, getPokemons] = useState(null);
    //const [imgpokemon, getImgpokemon] = useState('https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/back/101.png')
    const url = 'https://pokeapi.co/api/v2/pokemon?limit=15&offset=100'

    const getimg = async (pkmnurl) => {
        try {
            let res = await fetch(pkmnurl)
            let data = await res.json()
            //console.log(data.sprites.back_default)
            return data.sprites.back_default
        } catch (e) {
            console.log(e)
        }
    }

    useEffect(() => {
        fetch(url)
            .then(res => res.json())
            .then(async data => {
                for (let i = 0; i < data.results.length; i++) {
                    //console.log(data.results[i].url)
                    data.results[i].image = await getimg(data.results[i].url)
                    //console.log(data.results[i].image)
                }
                //console.log(data.results)
                getPokemons(data.results);
            })
            .catch(e => console.log(e))
    }, [url])

    return (
        <Row>
            <Col size="5">
                <Row>
                {pokemons && pokemons.map((pokemon, i) => (
                    <PokemonCard pokemon={pokemon} keyid={i} />
                ))}
                </Row>
            </Col>
        </Row>
    )
}

export default Inicio