import { createGlobalStyle } from "styled-components"

export const light = {
	foreground: "#000",
	background: "#eeeeee",
  shadowColor: "rgba(0, 0, 0, 0.15)",
  navColor: "#229",
  navColorHover: "#11B",
  bgFooterColor: "#0281CE"
}
export const dark = {
	foreground: "#fff",
	background: "#222222",
  shadowColor: "rgba(255, 255, 255, 0.15)",
  navColor: "#DDA",
  navColorHover: "#EE5",
  bgFooterColor: "#001066"
}


const GlobalStyle = createGlobalStyle`
    body {
      background-color: ${props => (props.light ? "white" : "black")};
    }
  `
export default GlobalStyle
