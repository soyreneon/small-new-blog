import styled from "styled-components"
import { NavLink } from "react-router-dom"
import ButtonTheme from './ButtonTheme'

const Nav = styled.nav`
    border-bottom: 1px solid #BBB;
    padding: 5px 45px;

    & ul {
        list-style-type: none;
        display: flex;
        padding: 0%;
        flex-direction: row;
        justify-content: center;
    }
    & li {
        flex: 2;
        text-align: center;
    }
    & a {
        color: ${props => props.theme.navColor};
        text-decoration: none;
        font-weight: 600;

        &.active{
            color: ${props => props.theme.navColorHover};
        }

        &:hover {
            color: ${props => props.theme.navColorHover};
        }
    }
`

const NavSection = () => {
    return (
        <Nav>
            <ul>
                <li><NavLink activeClassName="active" to="/" exact>Inicio</NavLink></li>
                <li><NavLink activeClassName="active" to="/mas">Más</NavLink></li>
                <li><ButtonTheme /></li>
            </ul>
        </Nav>
    )
}

export default NavSection