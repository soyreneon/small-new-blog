import styled from 'styled-components'

const Containery = styled.div`
    max-width: 1140px;
    margin: 0 auto;`

const Container = ({children}) => {
    return ( 
        <Containery>{children}</Containery>
     );
}

export default Container;