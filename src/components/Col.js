import styled from 'styled-components'
const Coly = styled.div`
        flex: ${({ size }) => (size)};`
const Col = ({ children, tagType='div', size='0' }) => {
    return (
        <Coly size={size}>{children}</Coly>
    )
}

export default Col