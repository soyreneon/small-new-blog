import styled from "styled-components"
let Rowy = styled.div`
	display: flex;
	flex-wrap: wrap;
	justify-content: center;
`
const Row = ({ children }) => {
	return <Rowy>{children}</Rowy>
}

export default Row
