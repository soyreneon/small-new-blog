import styled, { keyframes } from "styled-components";
const rotate = keyframes`
  from {
    transform: rotate(0deg);
  }

  to {
    transform: rotate(360deg);
  }
`;
const scale = keyframes`
  80% {
    transform: scale(1);
  }

  100% {
    transform: scale(1.4);
  }
`;
const Card = styled.div`
  min-height: 150px;
  width: 100%;
  /*width: 17%;*/
  box-shadow: 0 0.5rem 1rem ${props => props.theme.shadowColor};
  border: 1px solid ${props => props.theme.foreground};
  margin: 15px 15px;
  padding: 0 25px;
  box-sizing: content-box;

  &:hover {
    //background-color: #abc;

    img {
      animation: ${scale} 2s ease-in alternate infinite;
    }
  }

  & svg {
    height: 15px;
    width: 15px;
    animation: ${rotate} 2s linear infinite;
    margin-right: 4px;
    color: red;
    padding: 2px;
    background-color: yellow;
    border-radius: 18px;
  }

  & img {
    display: block;
    margin: 0 auto;
  }

  & h2 {
    text-align: center;
    font-weight: 700;
    margin-bottom: 2px;
    font-size: 18px;
    color: ${props => props.theme.foreground};
  }

  @media only screen and (min-width: 576px) {
    width: 100%;
  }
  @media only screen and (min-width: 768px) {
    width: 39%;
  }
  @media only screen and (min-width: 992px) {
    width: 25%;
  }
  /*
  @media only screen and (min-width: 1200px) {
    .colored {
      background-color: #00d !important;
    }
  } 
  */
`;

const PokemonCard = ({ pokemon, keyid }) => {
  //console.log(keyid)
  return (
    <Card  key={keyid.toString()} >
      <div>
        <h2>
          <i>
            <svg
              xmlns="http://www.w3.org/2000/svg"
              fill="none"
              viewBox="0 0 24 24"
              stroke="currentColor"
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                strokeWidth="2"
                d="M11.049 2.927c.3-.921 1.603-.921 1.902 0l1.519 4.674a1 1 0 00.95.69h4.915c.969 0 1.371 1.24.588 1.81l-3.976 2.888a1 1 0 00-.363 1.118l1.518 4.674c.3.922-.755 1.688-1.538 1.118l-3.976-2.888a1 1 0 00-1.176 0l-3.976 2.888c-.783.57-1.838-.197-1.538-1.118l1.518-4.674a1 1 0 00-.363-1.118l-3.976-2.888c-.784-.57-.38-1.81.588-1.81h4.914a1 1 0 00.951-.69l1.519-4.674z"
              />
            </svg>
          </i>
          {pokemon.name}
        </h2>
        {/*aqui esta el problema sin resolver*/}
        <img src={pokemon.image} alt={pokemon.name} />
      </div>
    </Card>
  );
};

export default PokemonCard;
