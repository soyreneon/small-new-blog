import styled from 'styled-components'
import Row from './Row'

const Header = styled.div`
    height: 240px;
    width: 100%;
    background-image: url(${({ img }) => (img || 'https://picsum.photos/id/1018/1200/1900')});
    background-position: center ;
    background-size: cover;
    background-repeat: no-repeat;
    display: flex;
    flex-direction: row;
    justify-content: center;
    align-items: center;

    & div > h1 {
        color: white;
        margin: 0;
    }
    & div > h2 {
        color: #CCC;
        text-align: center;
        margin: 0;
    }
    `
const HeaderSection = () => {
    return (
        <Row>
            <Header img='https://picsum.photos/id/1015/1200/1900'>
                <div>
                    <h1>this is my site</h1>
                    <h2>look at this</h2>
                </div>
            </Header>
        </Row>
    )
}
export default HeaderSection