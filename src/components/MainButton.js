import styled, { css } from 'styled-components'

const Button = styled.button`
      background: transparent;
      border-radius: 3px;
      border: 2px solid #6D6;
      color: #6D6;
      margin: 0.5em 1em;
      padding: 0.25em 1em;

      & > svg{
        height: 20px;
        width: 20px;
        margin-right: 10px;
        display: inline;
      }

      &:hover {
        background-color: #4D4;
        color: white;
      }

      ${props => props.primary && css`
        background: #6D6;
        color: white;
      
        &:hover {
          background-color: #4E4;
        }
      `}
  `;
const MainButton = ({ children, ...attributes }) => {
  return (
    <Button {...attributes}>{children}</Button>
  )
}
export default MainButton